#pragma once

#include <aaplus/AADate.h>

// ------------

void ui_print_date(CAADate *Date)
{
  auto JD = Date->Julian();

  printf("%ld-%02ld-%02ld %02ld:%02ld:%05.2f => %0.4f\n",
         Date->Year(), Date->Month(), Date->Day(),
         Date->Hour(), Date->Minute(), Date->Second(),
         JD);
}

// ------------

struct Transform
{
  long double cx;    // origin X (screen space)
  long double cy;    // origin Y (screen space)
  long double scale; // scale

  /**
   * Convert (x, y) coordinate to screen space
   */
  Pos2D screen(double x, double y)
  {
    return {cx + x * scale, cy + y * scale};
  }
};

// ------------

constexpr int SUN_RADIUS = 4;
constexpr int PLANET_RADIUS_SCALE = 15000;

#define RGBA_BACKGROUND 32, 32, 32, 255
#define RGBA_MOON 192, 192, 192, 255
#define RGBA_SUN 230, 230, 64, 255
#define RGBA_PLANET_ORBIT 96, 73, 42, 64
#define RGBA_PLANET 170, 170, 210, 255
#define RGB_TEXT 255, 255, 255

// ------------

#include <iomanip>
#include <iostream>
#include <sstream>

#include <SDL2/SDL.h>
#include <SDL2/SDL2_gfxPrimitives.h>
#include <SDL2/SDL_ttf.h>
#include <fontconfig/fontconfig.h>

SDL_Window *window = nullptr;
SDL_Renderer *render = nullptr;
TTF_Font *sans = nullptr;
SDL_Event event;

std::string ui_find_font()
{
  std::string out;

  auto *pat = FcNameParse((FcChar8 *)"FreeMono");
  if (!pat)
  {
    std::cerr << "Could not create font pattern" << std::endl;
    return out;
  }
  auto *os = FcObjectSetCreate();
  FcObjectSetAdd(os, "file");

  auto *fs = FcFontList(0, pat, os);

  FcObjectSetDestroy(os);
  FcPatternDestroy(pat);

  for (size_t i = 0; i < fs->nfont; ++i)
  {
    auto *font = fs->fonts[i];
    FcChar8 *ff = FcPatternFormat(font, (FcChar8 *)"%{file}");
    out = std::string((char *)ff);
    break;
  }

  FcFontSetDestroy(fs);
  FcFini();

  return out;
}

int ui_init()
{
  if (SDL_Init(SDL_INIT_VIDEO) != 0)
  {
    std::cerr << "could not init sdl2: " << SDL_GetError() << std::endl;
    return 1;
  }

  window = SDL_CreateWindow(
      "orrery",
      SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
      800, 600,
      SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI | SDL_WINDOW_OPENGL);

  if (window == nullptr)
  {
    std::cerr << "could not create window: " << SDL_GetError() << std::endl;
    return 1;
  }

  render = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
  if (render == nullptr)
  {
    std::cerr << "could not create renderer: " << SDL_GetError() << std::endl;
    return 1;
  }

  const auto ff = ui_find_font();
  if (ff.size() == 0)
  {
    std::cerr << "could not find font: " << std::endl;
    return 1;
  }

  if (TTF_Init() != 0)
  {
    std::cerr << "could not initialise ttf: " << SDL_GetError() << std::endl;
    return 1;
  }

  sans = TTF_OpenFont(ff.c_str(), 25);
  if (sans == nullptr)
  {
    std::cerr << "could not open font: " << SDL_GetError() << std::endl;
    return 1;
  }

  SDL_SetRenderDrawColor(render, RGBA_BACKGROUND);
  SDL_RenderClear(render);
  SDL_RenderPresent(render);
  SDL_SetRenderDrawBlendMode(render, SDL_BLENDMODE_BLEND);

  return 0;
}

int ui_cleanup(int returnCode)
{
  if (render != nullptr)
  {
    SDL_DestroyRenderer(render);
  }
  if (window != nullptr)
  {
    SDL_DestroyWindow(window);
  }
  SDL_Quit();
  return returnCode;
}

int ui_process_events()
{
  while (SDL_PollEvent(&event))
  {
    if (event.type == SDL_QUIT)
    {
      return 1;
    }
    if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE)
    {
      return 1;
    }
  }
  return 0;
}

void ui_render_moon(double d, Transform t, OrbitalObject *m, long double insetscale)
{
  // Draw the moon
  auto el = moon->calculate(d);
  // Draw the average orbit circle
  aacircleRGBA(render, t.cx, t.cy, el.r * t.scale - 0.5, RGBA_PLANET_ORBIT);
  aacircleRGBA(render, t.cx, t.cy, el.r * t.scale + 0.5, RGBA_PLANET_ORBIT);
  auto p = t.screen(el.p3.x, el.p3.y);
  auto mel = m->calculate(d);
  filledCircleRGBA(render, p.x, p.y, mel.radius * PLANET_RADIUS_SCALE * insetscale, RGBA_MOON);
}

void ui_render_earth_moon_inset(double d, double w, double h, Transform t, OrbitalObject *e, OrbitalObject *m, long double insetscale)
{
  // Draw box around the inset
  rectangleRGBA(render, t.cx - w / 2, t.cy - h / 2, t.cx + w / 2, t.cy + h / 2, RGBA_PLANET_ORBIT);
  rectangleRGBA(render, t.cx - w / 2 + 0.5, t.cy - h / 2 + 0.5, t.cx + w / 2 - 0.5, t.cy + h / 2 - 0.5, RGBA_PLANET_ORBIT);
  // Draw earth in inset origin
  auto eel = e->calculate(d);
  filledCircleRGBA(render, t.cx, t.cy, eel.radius * PLANET_RADIUS_SCALE * insetscale, RGBA_PLANET);
  ui_render_moon(d, t, m, insetscale);
}

void ui_render_planets(double d, Transform t, SolarSystem planets)
{
  // sun
  filledCircleRGBA(render, t.cx, t.cy, SUN_RADIUS, RGBA_SUN);

  // planets
  for (const auto &[name, planet] : planets)
  {
    auto el = planet->calculate(d);
    // Draw the average orbit circle
    aacircleRGBA(render, t.cx, t.cy, el.r * t.scale - 0.5, RGBA_PLANET_ORBIT);
    aacircleRGBA(render, t.cx, t.cy, el.r * t.scale - 0.0, RGBA_PLANET_ORBIT);
    aacircleRGBA(render, t.cx, t.cy, el.r * t.scale + 0.5, RGBA_PLANET_ORBIT);

    auto p = t.screen(el.p3.x, el.p3.y);
    if (name == "Earth")
    {
      // Flip the coords because we're using Sun data as a proxy for Earth
      p = t.screen(-el.p3.x, -el.p3.y);

      // Draw Earth-moon system in an inset; bottom left, 300px
      auto wi = 300.0;
      auto hi = 300.0;
      int ww, wh;
      SDL_GetWindowSize(window, &ww, &wh);
      auto tiscale = wi * 150;
      Transform ti{wi / 2, wh - (hi / 2), tiscale};
      ui_render_earth_moon_inset(d, wi, hi, ti, planet, moon, 20);
    }
    // Draw the planet position
    filledCircleRGBA(render, p.x, p.y, el.radius * PLANET_RADIUS_SCALE, RGBA_PLANET);
  }
}

int ui_render(int frame, double time, CAADate *date, double d, SolarSystem planets)
{
  // reset background
  {
    SDL_SetRenderDrawColor(render, RGBA_BACKGROUND);
    SDL_RenderFillRect(render, NULL);
  }

  int ww, wh;
  SDL_GetWindowSize(window, &ww, &wh);

  // render planets
  {
    auto HSH = wh / 2.0;
    auto HSW = ww / 2.0;
    const auto scale = 0.03 * HSH;
    Transform t{HSW, HSH, scale};
    ui_render_planets(d, t, planets);
  }

  // stats
  {
    std::stringstream stats;
    stats.precision(3);
    stats.fill(' ');
    stats << date->Year()
          << "-" << std::setfill('0') << std::setw(2) << date->Month()
          << "-" << std::setfill('0') << std::setw(2) << date->Day()
          << " " << std::setfill('0') << std::setw(2) << date->Hour()
          << ":" << std::setfill('0') << std::setw(2) << date->Minute()
          << ":" << std::setfill('0') << std::setw(2) << round(date->Second())
          << " UTC";
    SDL_Color txtc{RGB_TEXT};
    SDL_Surface *txts = TTF_RenderText_Solid(sans, stats.str().c_str(), txtc);
    SDL_Texture *txtt = SDL_CreateTextureFromSurface(render, txts);
    SDL_Rect txtbg{0, 0, ww, txts->h + 10};
    SDL_SetRenderDrawColor(render, RGBA_BACKGROUND);
    SDL_RenderFillRect(render, &txtbg);
    SDL_Rect txtp{25, 5, txts->w, txts->h};
    SDL_RenderCopy(render, txtt, NULL, &txtp);
    SDL_DestroyTexture(txtt);
    SDL_FreeSurface(txts);
  }

  // update
  SDL_RenderPresent(render);

  return 0;
}

const char *ui_get_error()
{
  return SDL_GetError();
}