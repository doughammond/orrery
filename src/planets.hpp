#pragma once

#include <list>
#include <map>
#include <math.h>

struct Pos2D
{
  long double x;
  long double y;
};

struct Pos3D
{
  long double x;
  long double y;
  long double z;
};

constexpr double DEGRAD = M_PI / 180.0;
constexpr double RADDEG = 180.0 / M_PI;

double E(long double M, long double e)
{
  auto iter = [&e](double a) {
    auto M = a * DEGRAD;
    return a + e * (180.0 / M_PI) * sin(M) * (1.0 + e * cos(M));
  };

  return iter(M);
}

Pos2D position2(long double a, long double E, long double e)
{
  auto Er = E * DEGRAD;
  auto x = a * (cos(Er) - e);
  auto y = a * (sqrt(1.0 - e * e) * sin(Er));
  return {x, y};
}

Pos3D position3(long double r, long double N, long double v, long double w, long double i)
{
  auto nr = N * DEGRAD;
  auto ir = i * DEGRAD;
  auto wr = w * DEGRAD;
  auto vr = v * DEGRAD;
  auto x = r * (cos(nr) * cos(vr + wr) - sin(nr) * sin(vr + wr) * cos(ir));
  auto y = r * (sin(nr) * cos(vr + wr) + cos(nr) * sin(vr + wr) * cos(ir));
  auto z = r * (sin(vr + wr) * sin(ir));
  return {x, y, z};
}

double v(Pos2D p)
{
  return atan2(p.y, p.x) * RADDEG;
}

double r(Pos2D p)
{
  return sqrt(p.x * p.x + p.y * p.y);
}

double rev(double x)
{
  auto r = x - floor(x / 360.0) * 360.0;
  if (r < 0)
  {
    return 360 + r;
  }
  else
  {
    return r;
  }
}

#define OE(ELEM, calc) \
  virtual long double ELEM(double d) { return calc; };

struct Elements
{
  long double N;
  long double i;
  long double w;
  long double a;
  long double e;
  long double M;
  long double radius;
  long double E;
  long double v;
  long double r;
  Pos2D p2;
  Pos3D p3;
};

class OrbitalObject
{
public:
  virtual long double N(double d) = 0;
  virtual long double i(double d) = 0;
  virtual long double w(double d) = 0;
  virtual long double a(double d) = 0;
  virtual long double e(double d) = 0;
  virtual long double M(double d) = 0;
  virtual long double radius(double d) = 0;
  virtual Elements calculate(double d)
  {
    Elements el;
    el.N = rev(N(d));
    el.i = rev(i(d));
    el.w = rev(w(d));
    el.a = a(d);
    el.e = e(d);
    el.M = rev(M(d));
    el.radius = radius(d);
    el.E = rev(E(el.M, el.e));
    el.p2 = position2(el.a, el.E, el.e);
    el.v = rev(v(el.p2));
    el.r = r(el.p2);
    el.p3 = position3(el.r, el.N, el.v, el.w, el.i);
    return el;
  }
};

constexpr long double AUKM = 1.496e8;

class Sun : public OrbitalObject // Geocentric; // == Earth Heliocentric? (inverted?)
{
  OE(N, 0.0)
  OE(i, 0.0)
  OE(w, 282.9404 + 4.70935e-5 * d)
  OE(a, 1.0)
  OE(e, 0.016709 - 1.151e-9 * d)
  OE(M, 356.0470 + 0.9856002585 * d)
  OE(radius, 6371 / AUKM) // Earth, AU
};

class Moon : public OrbitalObject // Geocentric
{
  OE(N, 125.1228 - 0.0529538083 * d)
  OE(i, 5.1454)
  OE(w, 318.0634 + 0.1643573223 * d)
  // OE(a, 60.2666) // (Earth radii) == 60.2666 * 6371km == 383958.5086km == 0.00261374069843 AU
  OE(a, 0.00261374069843) // (AU)
  OE(e, 0.054900)
  OE(M, 115.3654 + 13.0649929509 * d)
  OE(radius, 1737.1 / AUKM) // AU
};

class Mercury : public OrbitalObject // Heliocentric
{
  OE(N, 48.3313 + 3.24587e-5 * d)
  OE(i, 7.0047 + 5.00e-8 * d)
  OE(w, 29.1241 + 1.01444e-5 * d)
  OE(a, 0.387098) // AU
  OE(e, 0.205635 + 5.59e-10 * d)
  OE(M, 168.6562 + 4.0923344368 * d)
  OE(radius, 2439.7 / AUKM) // AU
};

class Venus : public OrbitalObject // Heliocentric
{
  OE(N, 76.6799 + 2.46590e-5 * d)
  OE(i, 3.3946 + 2.75e-8 * d)
  OE(w, 54.8910 + 1.38374e-5 * d)
  OE(a, 0.723330) // AU
  OE(e, 0.006773 - 1.302e-9 * d)
  OE(M, 48.0052 + 1.6021302244 * d)
  OE(radius, 6051.8 / AUKM) // AU
};

class Mars : public OrbitalObject // Heliocentric
{
  OE(N, 49.5574 + 2.11081e-5 * d)
  OE(i, 1.8497 - 1.78e-8 * d)
  OE(w, 286.5016 + 2.92961e-5 * d)
  OE(a, 1.523688) // AU
  OE(e, 0.093405 + 2.516e-9 * d)
  OE(M, 18.6021 + 0.5240207766 * d)
  OE(radius, 3389.5 / AUKM) // AU
};

class Jupiter : public OrbitalObject // Heliocentric
{
  OE(N, 100.4542 + 2.76854e-5 * d)
  OE(i, 1.3030 - 1.557e-7 * d)
  OE(w, 273.8777 + 1.64505e-5 * d)
  OE(a, 5.20256) // AU
  OE(e, 0.048498 + 4.469e-9 * d)
  OE(M, 19.8950 + 0.0830853001 * d)
  OE(radius, 69911 / AUKM) // AU
};

class Saturn : public OrbitalObject // Heliocentric
{
  OE(N, 113.6634 + 2.38980e-5 * d)
  OE(i, 2.4886 - 1.081e-7 * d)
  OE(w, 339.3939 + 2.97661e-5 * d)
  OE(a, 9.55475) // AU
  OE(e, 0.055546 - 9.499e-9 * d)
  OE(M, 316.9670 + 0.0334442282 * d)
  OE(radius, 58232 / AUKM) // AU
};

class Uranus : public OrbitalObject // Heliocentric
{
  OE(N, 74.0005 + 1.3978e-5 * d)
  OE(i, 0.7733 + 1.9e-8 * d)
  OE(w, 96.6612 + 3.0565e-5 * d)
  OE(a, 19.18171 - 1.55e-8 * d) // AU
  OE(e, 0.047318 + 7.45e-9 * d)
  OE(M, 142.5905 + 0.011725806 * d)
  OE(radius, 25362 / AUKM) // AU
};

class Neptune : public OrbitalObject // Heliocentric
{
  OE(N, 131.7806 + 3.0173e-5 * d)
  OE(i, 1.7700 - 2.55e-7 * d)
  OE(w, 272.8461 - 6.027e-6 * d)
  OE(a, 30.05826 + 3.313e-8 * d) // AU
  OE(e, 0.008606 + 2.15e-9 * d)
  OE(M, 260.2471 + 0.005995147 * d)
  OE(radius, 24622 / AUKM) // AU
};

typedef std::list<std::pair<std::string, OrbitalObject *>> SolarSystem;

SolarSystem planets{
    {"Mercury", new Mercury()},
    {"Venus", new Venus()},
    {"Earth", new Sun()}, // ?
    {"Mars", new Mars()},
    {"Jupiter", new Jupiter()},
    {"Saturn", new Saturn()},
    {"Uranus", new Uranus()},
    {"Neptune", new Neptune()}};

Moon *moon = new Moon();
