#include <chrono>
#include <stdio.h>

#include <aaplus/AADate.h>

#include "planets.hpp"
#include "ui.hpp"

typedef std::chrono::system_clock::time_point tp;
const auto now = std::chrono::system_clock::now;
const auto dt = [](tp begin, tp end) {
  return std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();
};

int ui_main()
{
  if (ui_init() != 0)
  {
    return ui_cleanup(1);
  }

  tp t_start = now();
  tp t_iter = t_start;

  long f = 0;
  double t = 0;
  for (;; ++f, t_iter = now(), t = dt(t_start, t_iter) / 1000.0)
  {
    if (ui_process_events() != 0)
    {
      return ui_cleanup(1);
    }

    auto ctime = std::chrono::system_clock::to_time_t(t_iter);
    auto cdate = std::gmtime(&ctime);

    auto Date = new CAADate(
        cdate->tm_year + 1900, cdate->tm_mon + 1, cdate->tm_mday,
        cdate->tm_hour, cdate->tm_min, cdate->tm_sec,
        true);
    auto d = Date->Julian() - 2451543.5;

    // printf("Sys date %s", std::ctime(&ctime));
    // ui_print_date(Date);

    if (ui_render(f, t, Date, d, planets) != 0)
    {
      std::cerr << "error rendering: " << ui_get_error() << std::endl;
      return ui_cleanup(1);
    }

    SDL_Delay(50);
  }

  return ui_cleanup(0);
}

int text_main()
{
  auto Date = new CAADate(2004, 1, 1, 0, 0, 0, true);
  auto JD = Date->Julian();
  auto d0 = 2451543.5;
  auto d = JD - d0;

  ui_print_date(Date);
  printf("d0 = %0.2f : d = %0.2f\n", d0, d);
  printf(
      "%-10s : %-9s %-9s %-9s %-9s %-9s %-9s %-9s %-9s %-9s %-9s %-9s %-9s\n",
      "Name",
      "N/deg", "i/deg", "w/deg", "a/a.e", "e", "M/deg", "E/deg", "r", "v/deg", "x", "y", "z");

  for (const auto &[name, planet] : planets)
  {
    auto el = planet->calculate(d);
    printf(
        "%-10s : %09.5Lf %09.5Lf %09.5Lf %09.5Lf %09.5Lf %09.5Lf %09.5Lf %09.5Lf %09.5Lf %09.5Lf %09.5Lf %09.5Lf\n",
        name.c_str(),
        el.N, el.i, el.w, el.a, el.e, el.M, el.E, el.r, el.v,
        el.p3.x, el.p3.y, el.p3.z);
  }

  return 0;
}

int main()
{
  return ui_main();
}